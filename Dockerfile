#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /apptechu

#Copiado de archivos
ADD build/default /apptechu/build/default
ADD server.js /apptechu
ADD package.json /apptechu

#Dependencias
RUN npm install

#Puerto expuesto
EXPOSE 3000

#Comando de ejecucion
CMD ["npm", "start"]
